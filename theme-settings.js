$(document).ready( function() {
  // Hide the breadcrumb details, if no breadcrumb.
  $('#edit-ad_redoable-breadcrumb').change(
    function() {
      div = $('#div-ad_redoable-breadcrumb-collapse');
      if ($('#edit-ad_redoable-breadcrumb').val() == 'no') {
        div.slideUp('slow');
      } else if (div.css('display') == 'none') {
        div.slideDown('slow');
      }
    }
  );
  if ($('#edit-ad_redoable-breadcrumb').val() == 'no') {
    $('#div-ad_redoable-breadcrumb-collapse').css('display', 'none');
  }
  $('#edit-ad_redoable-breadcrumb-title').change(
    function() {
      checkbox = $('#edit-ad_redoable-breadcrumb-trailing');
      if ($('#edit-ad_redoable-breadcrumb-title').attr('checked')) {
        checkbox.attr('disabled', 'disabled');
      } else {
        checkbox.removeAttr('disabled');
      }
    }
  );
  $('#edit-ad_redoable-breadcrumb-title').change();
} );
