<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/2002/REC-xhtml1-20020801/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language; ?>" xml:lang="<?php print $language->language; ?>">

<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <!--[if IE 7]>
    <link rel="stylesheet" href="<?php print $base_path . $directory; ?>/ie7.css" type="text/css">
  <![endif]-->
  <!--[if lte IE 6]>
    <link rel="stylesheet" href="<?php print $base_path . $directory; ?>/ie.css" type="text/css">
  <![endif]-->
  <?php print $scripts; ?>
</head>

<body class="<?php print $body_classes; echo " "; if ($primary_links) echo "primary-yes" ; else echo "primary-no" ?>">
  <div id="page" class="<?php if ($secondary_links) echo "secondary-yes" ; else echo "secondary-no" ?>"><div id="page-inner">

<div id="header">


<div id="header-inner" class="clear-block">

           <?php if ($search_box): ?>
            <div id="search-box">
              <?php print $search_box; ?>
            </div> <!-- /#search-box -->
          <?php endif; ?>

      <?php if ($logo || $site_name || $site_slogan): ?>
        <div id="logo-title"><div id="logo-title2"><div id="logo-title3">

          <?php if ($logo): ?>
            <div id="logo"><a href="<?php print $base_path; ?>" title="<?php print t('Home'); ?>" rel="home"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" id="logo-image" /></a></div>
          <?php endif; ?>

          <?php if ($site_name): ?>
            <h1 id="site-name">
              <a href="<?php print $base_path; ?>" title="<?php print t('Home'); ?>" rel="home">
                <strong><?php print $site_name; ?></strong>
              </a>
            </h1>
          <?php endif; ?>

          <?php if ($site_slogan): ?>
            <div id='site-slogan'><?php print $site_slogan; ?></div>
          <?php endif; ?>

        </div></div></div> <!-- /#logo-title -->
      <?php endif; ?>

    </div></div> <!-- /#header-inner, /#header -->

    <div id="main"><div id="main-inner" class="clear-block<?php if ($search_box || $primary_links || $secondary_links || $navbar) { print ' with-navbar'; } ?>">

      <div id="content"><div id="content-inner"><div id="content-inner2">

        <?php if ($mission): ?>
          <div id="mission"><?php print $mission; ?></div>
        <?php endif; ?>

        <?php if ($content_top): ?>
          <div id="content-top" class="region region-content_top">
            <?php print $content_top; ?>
          </div> <!-- /#content-top -->
        <?php endif; ?>

        <?php if ($breadcrumb or $title or $tabs or $help or $messages): ?>
          <div id="content-header">
            <?php print $breadcrumb; ?>
            <?php if ($title): ?>
              <h1 class="title"><?php print $title; ?></h1>
            <?php endif; ?>
            <?php print $messages; ?>
            <?php if ($tabs): ?>
              <div class="tabs"><?php print $tabs; ?></div>
            <?php endif; ?>
            <?php print $help; ?>
          </div> <!-- /#content-header -->
        <?php endif; ?>

        <div id="content-area">
          <?php print $content; ?>
        </div>

        <?php if ($feed_icons): ?>
          <div class="feed-icons"><?php print $feed_icons; ?></div>
        <?php endif; ?>

        <?php if ($content_bottom): ?>
          <div id="content-bottom" class="region region-content_bottom">
            <?php print $content_bottom; ?>
          </div> <!-- /#content-bottom -->
        <?php endif; ?>

      </div></div> <!-- /#content-inner, /#content -->

    <div id="footer"><div id="footer-inner">     
            <div id="footer-message"><?php print $footer_message; ?>
      <span class="clear"></span>
      <a href="http://www.rockettheme.com/Templates/Free_Templates/Novus_-_Free_Joomla_Template/">Theme</a> <a href="http://www.avioso.com">port</a> sponsored by Duplika <a href="http://www.duplika.com">Web Hosting</a></div>

    </div></div> <!-- /#footer-inner, /#footer -->
      
      </div> <!-- /#content-inner2, /#content-inner, /#content -->


          <a name="navigation" id="navigation"></a>

          <?php if ($primary_links): ?>
            <div id="primary"><div id="primary-inner">
              <?php print theme('links', $primary_links); ?>
            </div></div> <!-- /#primary -->
          <?php endif; ?>

          <?php if ($secondary_links): ?>
            <div id="secondary"><div id="secondary-inner">
              <?php print theme('links', $secondary_links); ?>
            </div></div> <!-- /#secondary -->
          <?php endif; ?>

          <?php print $navbar; ?>


      <?php if ($left): ?>
        <div id="sidebar-left"><div id="sidebar-left-inner">
          <?php print $left; ?>
        </div></div> <!-- /#sidebar-left-inner, /#sidebar-left -->
      <?php endif; ?>

      <?php if ($right): ?>
        <div id="sidebar-right"><div id="sidebar-right-inner">
          <?php print $right; ?>
        </div></div> <!-- /#sidebar-right-inner, /#sidebar-right -->
      <?php endif; ?>


    </div></div> <!-- /#main-inner, /#main -->


    <?php if ($closure_region): ?>
    <span class="clear"></span>
      <div id="closure-blocks"><?php print $closure_region; ?></div>
    <?php endif; ?>

    <?php print $closure; ?>

  </div></div> <!-- /#page-inner, /#page -->

</body>
</html>
